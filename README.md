# Containerized Python App - Message with tkinter

Esta es una aplicación de demostración para correr una aplicación contenerizada.

## Ejecutar aplicación

```shell script
python app.py
```

Ejecutando la aplicaciónn se vera una GUI con uns breve descripción de las UBIs, así como cuatro botones que desplegarán un mensaje con una breve descripción de cada una de las UBIs.

![Alt text](resources/screenshot-gui.png "GUI")

Y al dar click sobre algún botón, va a mostrar un mensaje con una breve descripción de la UBI.

![Alt text](resources/screenshot-gui-message.png "GUI")


## Construir imagen

```shell script
podman build -t quay.io/rh-ee-savazque/python-tkinter-message-app:latest -f Containerfile .
```

## Descargar imagen

```shell script
podman pull quay.io/rh-ee-savazque/python-tkinter-message-app
```


## Ejecutar container

```shell script
podman run --rm -it -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix quay.io/rh-ee-savazque/python-tkinter-message-app:latest
```