# import everything from tkinter module
from tkinter import *
from tkinter.ttk import *
  
# import messagebox from tkinter module
import tkinter.messagebox

import sys
import os

if os.environ.get('DISPLAY','') == '':
    print('no display found. Using :0.0')
    os.environ.__setitem__('DISPLAY', ':0.0')


# Create Root Object
root = Tk()
root.title('Demo Python Tkinter Application')

p1 = PhotoImage(file = 'resources/Logo-Red_Hat-Hat_icon-Standard-RGB.png')

# Setting icon of master window
root.iconphoto(True, p1)



mainMessage = "The UBI images allow you to share container\n images with others. Four UBI images are offered:\n micro, minimal, standard, and init "

StandardMessage = '''
The standard images (named ubi) are designed for any application that runs on RHEL.
'''
InitMessage = '''
The UBI init images, named ubi-init, contain the systemd initialization system, making them useful for building images in which you want to run systemd services, such as a web server or file server. The init image contents are less than what you get with the standard images, but more than what is in the minimal images. 
'''
MinimalMessage = '''
The UBI minimal images, named ubi-minimal offer a minimized pre-installed content set and a package manager (microdnf`). As a result, you can use a Containerfile while minimizing the dependencies included in the image. 
'''
MicroMessage = '''
The ubi-micro is the smallest possible UBI image, obtained by excluding a package manager and all of its dependencies which are normally included in a container image. This minimizes the attack surface of container images based on the ubi-micro image and is suitable for minimal applications, even if you use UBI Standard, Minimal, or Init for other applications. The container image without the Linux distribution packaging is called a Distroless container image.
'''



option = None

def print_message():
    global option
    match option:
        case 1:
            # Create a messagebox showinfo
            tkinter.messagebox.showinfo("Standard", StandardMessage)
        case 2:
            # Create a messagebox showinfo
            tkinter.messagebox.showinfo("Init", InitMessage)
        case 3:
            # Create a messagebox showinfo
            tkinter.messagebox.showinfo("Minimal", MinimalMessage)
        case 4:
            # Create a messagebox showinfo
            tkinter.messagebox.showinfo("Micro", MicroMessage)

def load1():
    global option
    option = 1
    print_message()

def load2():
    global option
    option = 2
    print_message()

def load3():
    global option
    option = 3
    print_message()

def load4():
    global option
    option = 4
    print_message()

  
# Setting icon of master window
root.iconphoto(False, p1)

# Initialize tkinter window with dimensions 100x100            
#root.geometry('776x500')
root.geometry('876x600')
root.resizable(False, False)


C = Canvas(root, height=10, width=10)
C.pack(fill = "both", anchor=tkinter.CENTER, expand = True)
# Add image file
im = PhotoImage(file = "resources/Logo-Red_Hat-B-Standard-RGB.png")
  
bg = tkinter.Label(root, image=im)
bg.place(x=0, y=0, relwidth=1, relheight=1)



# Create style Object
style = Style()
 
style.configure('TButton', font = ('calibri', 20, 'bold'), borderwidth = '4')
 
# Changes will be reflected
# by the movement of mouse.
style.map('TButton', foreground = [('active', '!disabled', 'white')], background = [('active', 'red')])




Label_middle = tkinter.Label(root, text = mainMessage, font = ('calibri', 20, 'bold'), takefocus=True)
 
Label_middle.pack(ipadx=20, pady=20, anchor=tkinter.CENTER,  expand=True)


btn1 = Button(root, text = 'Standard', command = load1)
btn2 = Button(root, text = 'Init', command = load2)
btn3 = Button(root, text = 'Minimal', command = load3)
btn4 = Button(root, text = 'Micro', command = load4)


btn1.pack(ipadx=20, ipady=20, anchor=tkinter.CENTER,  expand=True)
btn2.pack(ipadx=20, ipady=20, anchor=tkinter.CENTER,  expand=True)
btn3.pack(ipadx=20, ipady=20, anchor=tkinter.CENTER,  expand=True)
btn4.pack(ipadx=20, ipady=20, anchor=tkinter.CENTER,  expand=True)


# Close button
btn3 = Button(root, text = 'Close', command = root.destroy) 
btn3.pack(ipadx=20, ipady=20, anchor=tkinter.SE,  expand=True)


root.mainloop()
